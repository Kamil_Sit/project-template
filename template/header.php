<?php
	include("inc/variables.php");
	include("inc/helpers.php");
?>
<!DOCTYPE html>
<html lang="pl">
<head>
	<title><?php echo $title ?></title>
	<meta name="description" content="<?php echo $description ?>">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<link rel="icon" href="assets/img/favicon.png">
	<link rel="stylesheet" href="assets/styles/preloader.css" type="text/css" media="all">
	<script type="text/javascript">
		var Preloader = function() {
			document.getElementById("loader").removeAttribute("class");
			document.getElementById("loader").style.opacity = "0";
		};
		window.onload = Preloader;
	</script>
</head>

<body>


	<div id="loader" class="loader"></div>
<!--[if lt IE 10]>
	<div class="alert alert-warning">
		You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
	</div>
<![endif]-->
	<?php // include('view/parts/image-extender.php'); ?>
	<?php include('view/parts/cookies.php'); ?>
	<!--<div id="psd"></div>
	<div id="psd_switch">PSD ON/OFF</div> -->
	<!-- <div id="go_to_top"></div> -->
<div class="wrapper">
	<header class="parent parent--header">
		<section class="row container">
			<?php  include('view/parts/nav.php'); ?>
		</section>
	</header>
