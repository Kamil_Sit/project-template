<?php 
/**
 * Adream Theme Helpers
 *
 * @package Adream
 */

/*======================================
=            Image function            =
======================================*/

function img($src="",$class=""){
	if(!empty($src)){
		$class = !empty($class) ? $class : " ";
		echo '<img class="image '.$class.'" src="assets/img/'.$src.'" alt="'.$src.'">';
	}
}
function bgimg($src=""){
	if(!empty($src)){
		echo 'style="background-image: url(assets/img/'.$src.')"';
	}
}

/*=====  End of Image function  ======*/

/*=============================================
=            Random text generator            =
=============================================*/

if(!function_exists('random_text')){
	function random_text($max=0){
		$max_r = !empty($max) ? $max : 399;
		$lorem = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci minima, laboriosam amet. Adipisci hic minima a pariatur nam, vero, repudiandae quaerat, repellendus totam voluptates quisquam aliquam placeat est. Qui rem reprehenderit architecto minima, quidem quam, molestias eos veniam, nulla distinctio, asperiores. Harum voluptatem omnis porro quibusdam. Assumenda facilis necessitatibus, modi!";
		$rand =  rand(50, $max_r);
		echo substr($lorem, 0, $rand); 
	}
}
/*=====  End of Random text generator  ======*/