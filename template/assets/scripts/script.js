import $				from 'jquery';

// import tabs 			from './tabs';
// import scrollTop 	from './scrollTop';
// import userScroll 	from './userScroll';
// import achorNav 		from './achorNav';
// import parallax 		from './parallax';
import cokies 			from './cokies';
// import wordSizer 	from './wordSizer';
// import gallery			from './gallery';

// $(()=>{
// 	new gallery();
// });

// Initialize tabs switcher
// $(()=>{
// 	new tabs(".tabs__nav", ".tabs__wrap");
// });

// Initialize ScrollTop
// $(()=>{
// 	new scrollTop();
// });

// Initialize user scroll stop achor, scroll top
// $(()=>{
// 	new userScroll();
// });

// Initialize achor navigation 
// $(()=>{
// 	new achorNav();
// });

// Initialize paralax
// $(()=>{
// 	new parallax("#parallax");
// });

// Initialize Cokies
$(()=>{
	new cokies();
});