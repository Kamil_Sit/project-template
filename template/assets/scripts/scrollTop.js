import $ 	from 'jquery';
export default function() {
	$(window).scroll(function(){
			if($(window).scrollTop() > 500){
				$("#go_to_top").fadeIn();
			}else{
				$("#go_to_top").fadeOut();
			}

		});
	
	$(document).on('click', '#go_to_top', function(event){
		$('html, body').animate({
			scrollTop: 0+'px'
		},2000);
	});
}