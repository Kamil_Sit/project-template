<?php include("header.php") ?>
	<main class="parent parent--content">
		<section class="row container">	
			<header class="header">
				<div class="header__text">
					<h2 class="header__text__h2">
						<hr><div>Need our help?</div><hr>
					</h2>

					<h1 class="header__text__h1">
						TOP DESIGNERS
					</h1>

					<a href="#"><button class="header__text__button">Check us!
					</button></a>
				</div>
				<div class="header__scroll">
				</div>
			</header>
		</section>
	</main>
<?php include("footer.php") ?>