'use strict';

import gulp			from 'gulp';
import config		from './config';
import reportErr	from './notify';
import plumber		from 'gulp-plumber';
import browserSync	from 'browser-sync';

import watch		from 'gulp-watch';

export default () => {
	return gulp.src(config.template_pattern)
			.on('error', function(error) {
				reportErr(error, this);
			})
			.pipe(gulp.dest(config.move_dest));
}
export let selectify = () => {
	return gulp.src( config.template_pattern )
		.on('error', function(error) {
			reportErr(error, this);
		})
		.pipe( watch( config.template_pattern, browserSync.reload ) )
		.pipe( gulp.dest(config.move_dest) );
}